export function determineDiceResults(dice, rollScore) { 
    for (let index = 0; index < 5; index++) {
      const die = dice[index]
      rollScore.results[die.value - 1]++
      rollScore.chance += die.value
    }

    return rollScore
  }